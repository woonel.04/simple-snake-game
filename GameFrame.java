package snakegame;

import javax.swing.*;
// import java.awt.event.ItemEvent;

public class GameFrame extends JFrame {
    GamePanel game;

    JButton resetButton;

    GameFrame() {
        GamePanel panel = new GamePanel();
        this.add(panel);
        this.setTitle("Snake");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.pack();
        this.setVisible(true);
        this.setLocationRelativeTo(null);
    }

}
